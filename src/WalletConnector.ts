import {
  ColonyNetworkClient,
  ColonyRole,
  getBlockTime,
  getColonyNetworkClient,
  getLogs,
  Network,
} from "@colony/colony-js";
import { utils, Wallet } from "ethers";
import { Filter, InfuraProvider, Log } from "ethers/providers";
import { ParsedLog } from "./types/Logs";

const MAINNET_NETWORK_ADDRESS: string = `0x5346D0f80e2816FaD329F2c140c870ffc3c3E2Ef`;
const MAINNET_BETACOLONY_ADDRESS: string = `0x869814034d96544f3C62DE2aC22448ed79Ac8e70`;
const tokens: {
  address: string;
  tokenName: string;
}[] = [
  {
    address: "0x6b175474e89094c44da98b954eedeac495271d0f",
    tokenName: "DAI",
  },
  {
    address: "0x0dd7b8f3d1fa88fabaa8a04a0c7b52fc35d4312c",
    tokenName: "BLNY",
  },
];

var colonyClient: any;
const wallet: Wallet = Wallet.createRandom();
const provider: InfuraProvider = new InfuraProvider();
const connectedWallet: Wallet = wallet.connect(provider);
const networkClient: ColonyNetworkClient = getColonyNetworkClient(
  Network.Mainnet,
  connectedWallet,
  {
    networkAddress: MAINNET_NETWORK_ADDRESS,
  }
);

export const getAllLogs = async (): Promise<Log[]> => {
  if (!colonyClient) {
    colonyClient = await networkClient.getColonyClient(
      MAINNET_BETACOLONY_ADDRESS
    );
  }

  const eventPayoutClaimedFilter: Filter = colonyClient.filters.PayoutClaimed(
    null,
    null,
    null
  );

  const eventColonyRoleSetFilter: Filter = colonyClient.filters.ColonyRoleSet(
    null,
    null,
    null,
    null
  );
  const eventColonyInitialisedFilter: Filter =
    colonyClient.filters.ColonyInitialised(null, null);
  const eventDomainAddedFilter: Filter = colonyClient.filters.DomainAdded(null);

  const eventPayoutClaimedLog: Log[] = await getLogs(
    colonyClient,
    eventPayoutClaimedFilter
  );
  const eventColonyRoleSetLog: Log[] = await getLogs(
    colonyClient,
    eventColonyRoleSetFilter
  );
  const eventColonyInitialisedLog: Log[] = await getLogs(
    colonyClient,
    eventColonyInitialisedFilter
  );
  const eventDomainAddedLog: Log[] = await getLogs(
    colonyClient,
    eventDomainAddedFilter
  );

  return eventPayoutClaimedLog.concat(
    eventColonyRoleSetLog,
    eventColonyInitialisedLog,
    eventDomainAddedLog
  );
};

const sortLogs = (logs: ParsedLog[]): ParsedLog[] =>
  logs.sort((prev, next) => next.logTime - prev.logTime);

export const parseLogs = async (logs: Log[]): Promise<ParsedLog[]> => {
  const parsedTimeLogs: Log[] = await Promise.all(
    logs.map(async (event: Log) => {
      const { blockHash } = event;
      const logTime: number = await getBlockTime(provider, blockHash || "");
      return { logTime, ...colonyClient.interface.parseLog(event) };
    })
  );

  const parsedLogs = await Promise.all(
    parsedTimeLogs.map(async (event: any) => {
      const { values, logTime } = event;
      const { amount, token, fundingPotId, role, user, domainId } = values;
      switch (event.name) {
        case "PayoutClaimed":
          const humanReadableFundingPotId = new utils.BigNumber(
            event.values.fundingPotId
          ).toString();
          const { associatedTypeId } = await colonyClient.getFundingPot(
            humanReadableFundingPotId
          );
          const { recipient: userAddress } = await colonyClient.getPayment(
            associatedTypeId
          );
          const wei = new utils.BigNumber(10);
          const humanReadableAmount = new utils.BigNumber(amount);
          const convertedAmount = humanReadableAmount.div(wei.pow(18));
          const tokenNames =
            tokens.filter(
              (q) => q.address === token.toString().toLowerCase()
            )[0].tokenName || token;

          return {
            logTime,
            address: userAddress,
            text: `User <b>${userAddress}</b> claimed <b>${convertedAmount} ${tokenNames}</b> payout from pot <b>${fundingPotId}</b>`,
          };
        case "ColonyRoleSet":
          return {
            logTime,
            address: user,
            text: `<b>${
              ColonyRole[role]
            }</b> role assigned to user <b>${user}</b> in domain <b>${parseInt(
              domainId._hex,
              16
            )}</b>`,
          };
        case "ColonyInitialised":
          return {
            logTime,
            address: colonyClient.address,
            text: "Congratulations! It's a beautiful baby colony!",
          };
        case "DomainAdded":
          return {
            logTime,
            address: colonyClient.address,
            text: `Domain <b>${parseInt(domainId._hex, 16)}</b> added.`,
          };
        default:
          return {};
      }
    })
  );
  return sortLogs(parsedLogs as ParsedLog[]);
};
