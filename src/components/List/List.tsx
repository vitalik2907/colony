import React, { FC, useState } from "react";
import Pagination from "@mui/material/Pagination";
import usePagination from "../../hooks/Pagination";
import Item from "../Item/Item";
import { ParsedLog } from "../../types/Logs";
import styles from "./List.module.css";

interface ListParams {
  logs: ParsedLog[];
}

const PER_PAGE = 9;

const List: FC<ListParams> = ({ logs }): JSX.Element => {
  const [page, setPage] = useState<number>(1);
  const count: number = Math.ceil(logs.length / PER_PAGE);
  const paginatedList = usePagination(logs, PER_PAGE);

  const handleChange = (e: React.ChangeEvent<any>, page: number) => {
    setPage(page);
    paginatedList.jump(page);
  };

  return (
    <>
      <div className={styles.list}>
        {paginatedList.currentData().map((item: ParsedLog, index: number) => (
          <Item key={index} item={item} />
        ))}
      </div>
      <div className={styles.pagination}>
        <Pagination
          count={count}
          size="large"
          page={page}
          variant="outlined"
          shape="rounded"
          onChange={handleChange}
        />
      </div>
    </>
  );
};

export default List;
