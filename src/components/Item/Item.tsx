import { FC } from "react";
import moment from "moment";
import parse from "html-react-parser";
import { Blockie } from "./Blockie";
import { ParsedLog } from "../../types/Logs";
import styles from "./Item.module.css";

interface ItemParams {
  item: ParsedLog;
}

const Item: FC<ItemParams> = ({ item }) => {
  const { logTime, text, address } = item;
  const date: string = moment(logTime).format("DD MMM");

  return (
    <>
      <div className={styles.list_item}>
        <Blockie address={address} />
        <div className={styles.info}>
          <p className={styles.text}>{parse(text)}</p>
          <div className={styles.date}>{date}</div>
        </div>
      </div>
    </>
  );
};

export default Item;
