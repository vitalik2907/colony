import { useMemo, FC } from "react";
import makeBlockie from "ethereum-blockies-base64";

interface BlockieParams {
  address: string;
}

export const Blockie: FC<BlockieParams> = ({ address }) => {
  const imgSrc: string = useMemo(() => {
    return makeBlockie(address);
  }, [address]);

  return <img src={imgSrc} alt="blockie" />;
};
