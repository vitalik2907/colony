import { useEffect, useState } from "react";
import { Log } from "ethers/providers";
import CircularProgress from "@mui/material/CircularProgress";
import List from "../List/List";
import { getAllLogs, parseLogs } from "../../WalletConnector";
import { ParsedLog } from "../../types/Logs";
import styles from "./App.module.css";

const App = (): JSX.Element => {
  const [logs, setLogs] = useState<ParsedLog[]>([]);

  useEffect(() => {
    const getLogs = async () => {
      const logs: Log[] = await getAllLogs();
      const parsedLogs: ParsedLog[] = await parseLogs(logs);
      setLogs(parsedLogs);
    };
    getLogs();
  }, []);

  return (
    <div className={styles.app}>
      <div className={styles.wrap}>
        {logs.length ? (
          <List logs={logs} />
        ) : (
          <div className={styles.loading}>
            <CircularProgress />
          </div>
        )}
      </div>
    </div>
  );
};

export default App;
