export type ParsedLog = {
  logTime: number;
  text: string;
  address: string;
};
