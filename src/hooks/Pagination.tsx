import { useState } from "react";
import { ParsedLog } from "../types/Logs";

const usePagination = (data: ParsedLog[], itemsPerPage: number) => {
  const [currentPage, setCurrentPage] = useState<number>(1);
  const maxPage: number = Math.ceil(data.length / itemsPerPage);

  const currentData = (): ParsedLog[] => {
    const begin: number = (currentPage - 1) * itemsPerPage;
    const end: number = begin + itemsPerPage;
    return data.slice(begin, end);
  };

  const next = (): void => {
    setCurrentPage((currentPage: number) => Math.min(currentPage + 1, maxPage));
  };

  const prev = (): void => {
    setCurrentPage((currentPage: number) => Math.max(currentPage - 1, 1));
  };

  const jump = (page: number): void => {
    const pageNumber: number = Math.max(1, page);
    setCurrentPage((currentPage: number) => Math.min(pageNumber, maxPage));
  };

  return { next, prev, jump, currentData, currentPage, maxPage };
};

export default usePagination;
